// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "atlantisGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ATLANTIS_API AatlantisGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
